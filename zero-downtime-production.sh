#!/bin/bash

# Variables
#
# Zero downtime var
old_container_id=$(docker ps -f name=$COMPOSEAPP_PROD -q | tail -n1)
new_container_id=$(docker ps -f name=$COMPOSEAPP_PROD -q | head -n1)
new_container_ip=$(docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $new_container_id)

# Nginx reload function
reload_nginx() {
    docker compose -f $COMPOSEFILE_PROD exec $COMPOSEWEBSERVERS_PROD /usr/sbin/nginx -s reload
}

# Zero downtime function
zero_downtime_deploy() {
    # bring a new container online, running new code
    # (nginx continues routing to the old container only)
    docker compose -f $COMPOSEFILE_PROD up -d --no-deps --scale $COMPOSEAPP_PROD=2 --no-recreate $COMPOSEAPP_PROD

    # wait for new container to be available
    curl -I --silent --include --retry-connrefused --retry 30 --retry-delay 1 --fail http://$new_container_ip:80/ || exit 1

    # start routing requests to the new container (as well as the old)
    reload_nginx

    # take the old container offline
    docker stop $old_container_id
    docker rm $old_container_id
    docker compose -f $COMPOSEFILE_PROD up -d --no-deps --scale $COMPOSEAPP_PROD=1 --no-recreate $COMPOSEAPP_PROD

    # stop routing requests to the old container
    reload_nginx
}

# Main
zero_downtime_deploy
