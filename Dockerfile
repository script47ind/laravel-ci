# Base image
FROM adhocore/phpfpm:8.0

# Install dependencies
RUN \
apk add -U --no-cache \
    bash \
    nano \
    nginx \
    supervisor \
    net-tools

# Nginx config
COPY nginx/nginx.conf /etc/nginx/nginx.conf
COPY nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf

# Add resource
ADD \
    laraschool-app \
    php/info.php \
    /var/www/html/

# Supervisor config
COPY \
    nginx/nginx.ini \
    php/php-fpm.ini \
    /etc/supervisor.d/

# Entrypoint
COPY \
    docker-entrypoint.sh \
    /docker-entrypoint.sh

# Update composer
RUN composer self-update --2

# Ports
EXPOSE 9000 80 443

# Permissions
RUN \
    chown -R www-data: storage bootstrap/cache public/img && \
    chmod +x /docker-entrypoint.sh

# Remove unnecessary files
RUN \
rm -rf \
    /var/cache/apk/* \
    /tmp/* /var/tmp/* \
    /usr/share/doc/* \
    /usr/share/man/*

# Commands
ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["supervisord", "-n", "-j", "/supervisord.pid"]